
# Git Cheat Sheet

A few command definitions, explanations, and reference examples

### Create new repo sequence

From a fork:

$ git clone git@gitlab.com:bennetthub500/2022-october-git-training.git
$ cd 2022-october-git-training/

$ git remote -v
origin  git@gitlab.com:bennetthub500/2022-october-git-training.git (fetch)
origin  git@gitlab.com:bennetthub500/2022-october-git-training.git (push)

$ git remote add upstream git@gitlab.com:tgdp/git-training/2022-october-git-training.git

You can now see:
$ git remote -v
origin  git@gitlab.com:bennetthub500/2022-october-git-training.git (fetch)
origin  git@gitlab.com:bennetthub500/2022-october-git-training.git (push)
upstream        git@gitlab.com:tgdp/git-training/2022-october-git-training.git (fetch)
upstream        git@gitlab.com:tgdp/git-training/2022-october-git-training.git (push)

$ git pull upstream main
From gitlab.com:tgdp/git-training/2022-october-git-training
 * branch            main       -> FETCH_HEAD
 * [new branch]      main       -> upstream/main
Already up to date.

Bennetts-MBP:2022-october-git-training bennettcharles$ git push origin main
Everything up-to-date



### File save sequence

$ git add --all
or
$ git add <filename>

$ git commit -m "write a message here"
# commits to local repo only - next you must push

$ git push origin your-branch-name


### Git staging area explanations
https://stackoverflow.com/questions/59179722/what-is-the-use-of-staging-area-in-git

https://www.developernation.net/blog/git-internals-part-3-understanding-the-staging-area-in-git



test addition